var users = {

    0: {
        name: "John",
        phoneNumber: 6363047777,
        location: {
            latitude: "51.606300",
            longitude: "-0.190509"
        },
        address: {
            city: "London",
            street: "88 Station Road London"

        }
    },

    1: {
        name: "Jane",
        phoneNumber: 927964,
        location: {
            latitude: "53.483718",
            longitude: "-2.229473"
        },
        address: {
            city: "Manchester",
            street: "42nd Street"
        }
    },

    2: {
        name: "Kevin",
        phoneNumber: 472131,
        location: {
            latitude: "48.880130",
            longitude: "2.331235"
        },
        address: {
            city: "Paris",
            street: "1 Rue Moncey"
        }
    },

    3: {
        name: "Ivan",
        phoneNumber: 224669,
        location: {
            latitude: "43.347848",
            longitude: "17.800062"
        },
        address: {
            city: "Mostar",
            street: "Kralja Tomislava"
        }
    },

    4: {
        name: "Michael",
        phoneNumber: 263422,
        location: {
            latitude: "40.764303",
            longitude: "-73.969486"
        },
        address: {
            city: "New York",
            street: "530 Park Ave",
        }
    },
}


const express = require('express')
const app = express()
const port = 3000

app.get('/users', (req, res) => res.send(users))

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
