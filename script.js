getData("http://localhost:3000/users", (response) => {
    console.log(response);

    // for (const key of Object.keys(response)) {
    //     users.push(response[key])
    // }

    const users = Object.keys(response).map((key) => response[key])
    //
    // console.log('--- Users ----')
    // console.log(users);


    // console.log('--- 2 - Users By Phone Number ----')
    // console.log(sortByPhoneNumber(users));

    // console.log('--- 3 - Users By Street Name ----')
    // console.log(sortByStreetName(users));

    // console.log('--- 4 Get Cities based on Compass ---')
    // getUsersBasedOnLocation(users, 'north')
    // getUsersBasedOnLocation(users, 'south')


    biggestRepetition(users);
});

/**
 1. Napišite funkciju za asinkroni dohvat podataka sa servera.
 Dohvatite JSON s https://api.myjson.com/bins/q6nae i preko callbacka pozovite obradu podataka. - 5 bodova
 */

function getData(url, cb) {
    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const response = JSON.parse(xhttp.response);
            cb(response);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

/**
 2.
 Napisite funkciju koja prima podatke koje ste dobili sa servera.
 Funkcija treba sortirati Usere po broju telefona od najmanjeg ka najvecem. - 15 bodova
 */
function sortByPhoneNumber(users) {
    // Use slice for deep copy (https://stackoverflow.com/questions/597588/how-do-you-clone-an-array-of-objects-in-javascript)
    // return users.slice().sort((a, b) => a.phoneNumber - b.phoneNumber)
    return [...users].sort((a, b) => a.phoneNumber - b.phoneNumber)
}

/**
 3. Napisite funkciju koja prima podatke koje ste dobili sa servera.
 Funkcija treba sortirati usere po nazivu grada. Tako sortiranim userima zamjeniti 'space' iz ulice sa znakom '+' i ispisati obrnuti naziv ulice. - 15 bodova
 */
function sortByStreetName(users) {
    return [...users]
        .sort((a, b) => {
            if (a.address.city < b.address.city) {
                return -1;
            }
            if (a.address.city > b.address.city) {
                return 1;
            }
            return 0;
        })
        .map((user) => {
            // user.address.street = user.address.street.split(' ').reverse().join('+')
            // return user;

            return Object.assign({}, user, {
                address: {
                    street: user.address.street.split(' ').reverse().join('+')
                }
            });
        })
}


/**
 4. Napisati funkciju koja prima dva parametra, podatke koje ste dobili sa servera i trazenu putanju (moze biti prema sjeveru il jugu).
 Funkcija bi treba ispisati sve gradove koji se nalaze ovisno o putanji od ekvatora.
 Funkcija se treba dva puta pozvati. Jednom s putanjom sjever, a drugi put s putanjom jug. - 20 bodova.
 */
function getUsersBasedOnLocation(users, compass) {
    const northUsers = []
    const southUsers = []

    users.forEach((user) => {
        const longitude = parseFloat(user.location.longitude)
        if (longitude > 0) {
            northUsers.push(user);
        } else if (longitude < 0) {
            southUsers.push(user);
        }
    })

    switch (compass) {
        case 'south':
            console.log('North: ', southUsers);
            break;
        case 'north':
            console.log('South: ', northUsers);
            break;
    }
}

/**
 5. Napravite funkciju koja ce primati podatke koje ste dobili sa servera.
 Nadjite koji user ima najvise ponavljanja nekog broja u svom broju telefona.
 Ispis bi treba da izgleda: Kod user-a {naziv user-a} najvise se ponavlja broj {nadjeniBroj} {kolikoPuta} puta. - 25 bodova
 */
function biggestRepetition(users) {
    let userWithBiggestRepetition = null;
    let max = -Infinity

    users.forEach((user) => {
        const biggestRepetition = biggestRepetitionNumber(user.phoneNumber);

        if (biggestRepetition.repetition > max) {
            max = biggestRepetition.repetition
            userWithBiggestRepetition = user;
            userWithBiggestRepetition.biggestRepetitionNumber = biggestRepetition
        }
    })

    console.log(`Kod user-a ${userWithBiggestRepetition.name} najvise se ponavlja broj ${userWithBiggestRepetition.biggestRepetitionNumber.number}, ${userWithBiggestRepetition.biggestRepetitionNumber.repetition} puta.`);
}

function biggestRepetitionNumber(phoneNumber) {
    let phoneMap = {}
    let phoneArray = phoneNumber.toString().split('');
    let max = -Infinity

    phoneArray.forEach((phoneChar) => {
        if (!phoneMap[phoneChar]) {
            phoneMap[phoneChar] = 1;
        } else {
            phoneMap[phoneChar] = phoneMap[phoneChar] + 1
        }
    })

    for (let key in phoneMap) {
        let number = parseInt(phoneMap[key]);
        if (number > max) {
            max = number;
            biggestRepetition = {number: key, repetition: phoneMap[key]}
        }
    }

    return biggestRepetition
}

/**
 Treba da napravite ovaj dizajn (aside i section moraju da budu float-ani): 20 bodova
 Elementi ne smiju da imaju parenta.
 Sliku mozete da pogledate na ovom linku - https://ibb.co/whM9VKh
 */
