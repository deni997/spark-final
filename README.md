## Setup

Install dependencies
```
npm install 
```
```
npm install http-server -g
```

Run the Node API
```
node server.js
``` 

Run http server in project folder (+ Disable CORS in your browser)

```
http-server
```